# Docker / my-ubuntu
Простой имидж ubuntu с небольшими добавками от себя.
## Получаем репу
```bash
git clone git@gitlab.com:kapuza-docker/my-ubuntu.git
cd my-ubuntu
git config user.name "Kaka Puzikova"; git config user.email "gitlab@tost.info.tm"
```
## Использование
```bash
# Сборка имиджа (если он уже склонирован)
docker build -t my-ubuntu .
# Если хотим прописать альтернативный репозиторий
docker build --build-arg UBUNTU_REP=mirror.yandex.ru -t my-ubuntu .

# Можно прям из репы (если лень клонировать)
docker build -t my-ubuntu git@gitlab.com:kapuza-docker/my-ubuntu.git
# Если хотим прописать альтернативный репозиторий
docker build --build-arg UBUNTU_REP=mirror.yandex.ru -t my-ubuntu git@gitlab.com:kapuza-docker/my-ubuntu.git

# Проверим его наличие
docker image ls

# Запуск контейнера
docker run -it my-ubuntu /bin/bash
```
