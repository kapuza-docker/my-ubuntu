FROM ubuntu:latest

MAINTAINER Kaka Puzikova <gitlab@tost.info.tm>

# Пропишим свою репу
#ARG UBUNTU_REP=mirror.yandex.ru
ARG UBUNTU_REP=repo.home.xmu

# Пропишем свой дистрибутив
ARG UBUNTU_DIST=xenial

COPY ./sysinstall /mnt/sysinstall
ENV DEBIAN_FRONTEND noninteractive
RUN chmod +x /mnt/sysinstall
RUN /mnt/sysinstall

RUN touch /var/log/syslog

#CMD tail -f /var/log/syslog
